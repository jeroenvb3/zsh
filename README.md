# My zsh config files

```bash
cd .config

git clone https://gitlab.com/jeroenvb3/zsh

cd zsh

ln -s $(pwd)/.oh-my-zsh $HOME/.oh-my-zsh
ln -s $(pwd)/.zshrc $HOME/.zshrc
ln -s $(pwd)/.zsh_aliases $HOME/.zsh_aliases
